﻿namespace Prototype.Abstractions
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}
