﻿using Faker;
using Prototype.Models;
namespace Prototype
{
    public static class Program
    {
        static void Main()
        {
            var pet = new Pet(7, 8, NameFaker.FirstName(), 2);
            var spider = new Spider(3, 0, NameFaker.FirstName(), 8, true);
            var cat = new Cat(1, 5, NameFaker.FirstName(), 4, true);
            var maineCoon = new MaineCoon(2, 10, NameFaker.FirstName(), 4, true, true);

            CheckClone<Pet>(pet);
            CheckClone<Spider>(spider);
            CheckClone<Cat>(cat);
            CheckClone<MaineCoon>(maineCoon);
        }

        private static void CheckClone<T>(Pet pet) where T : Pet
        {
            string type = typeof(T).Name;
            Console.WriteLine($"\nCheck Class [{type}]\n");
            var clonePet1 = (T)pet.Clone();
            var clonePet2 = pet.MyClone();
            PrintPets(type, pet, clonePet1, clonePet2);

            Console.WriteLine("\nChanges..");
            pet.Name = NameFaker.FirstName();
            clonePet1.Name = NameFaker.FirstName();
            clonePet2.Name = NameFaker.FirstName();
            PrintPets(type, pet, clonePet1, clonePet2);
        }

        private static void PrintPets<T>(string type, T origin, T clone1, T clone2) where T : Pet
        {
            Console.WriteLine($"[{type}] origin: {origin}\n" +
                              $"[{type}] clone1: {clone1}\n" +
                              $"[{type}] clone2: {clone2}");
        }
    }
}