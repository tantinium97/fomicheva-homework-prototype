﻿using Prototype.Abstractions;

namespace Prototype.Models
{
    public class MaineCoon : Cat, IMyCloneable<MaineCoon>
    {
        /// <summary>
        /// Признак наличия кисточек на ушах
        /// </summary>
        public bool IsEarTassels { get; set; }

        public MaineCoon() { }

        public MaineCoon(MaineCoon cat) : base(cat.Age, cat.Weight, cat.Name, cat.NumberOfPaws, cat.HasClaws)
        {
            IsEarTassels = cat.IsEarTassels;
        }

        public MaineCoon(int age, int weight, string name, int numberOfPaws, bool hasClaws, bool isEarTassels)
            : base(age, weight, name, numberOfPaws, hasClaws)
        {
            IsEarTassels = isEarTassels;
        }

        public override string AsksToEat() => "Мейн-Кун просит кушать";

        public override MaineCoon MyClone()
        {
            return new MaineCoon(this);
        }

        public override string ToString() =>
            $"Age: {Age} | Weight: {Weight} | Name: {Name} | NumberOfPaws: {NumberOfPaws} | IsEarTassels: {IsEarTassels} | HasClaws: {HasClaws}, {AsksToEat()}";
    }
}
