﻿using Prototype.Abstractions;

namespace Prototype.Models
{
    public class Spider : Pet, IMyCloneable<Spider>, ICloneable
    {
        /// <summary>
        /// Признак наличия яда
        /// </summary>
        public bool IsPoisonous { get; set; }

        public Spider() { }

        public Spider(Spider spider) : base(spider.Age, spider.Weight, spider.Name, spider.NumberOfPaws)
        {
            IsPoisonous = spider.IsPoisonous;
        }

        public Spider(int age, int weight, string name, int numberOfPaws, bool isPoisonous) : base(age, weight, name, numberOfPaws)
        {
            NumberOfPaws = numberOfPaws;
            IsPoisonous = isPoisonous;
        }

        public override string AsksToEat() => "Паучок просит кушать";

        public override Spider MyClone()
        {
            return new Spider(this);
        }

        public override string ToString() =>
            $"Age: {Age} | Weight: {Weight} | Name: {Name} | NumberOfPaws: {NumberOfPaws}, IsPoisonous: {IsPoisonous}, {AsksToEat()}";
    }
}
