﻿namespace Prototype.Models
{
    public class Pet
    {
        /// <summary>
        /// Возраст
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Вес
        /// </summary>
        public int Weight { get; set; }

        /// <summary>
        /// Кличка
        /// </summary>
        public string Name { get; set; } = "My lovely pet";

        /// <summary>
        /// Количесвто лап
        /// </summary>
        public int NumberOfPaws { get; set; }

        public Pet() {}

        public Pet(int age, int weight, string name, int numberOfPaws)
        {
            Age = age;
            Weight = weight;
            Name = name;
            NumberOfPaws = numberOfPaws;
        }

        public Pet(Pet pet)
        {
            Age = pet.Age;
            Weight = pet.Weight;
            Name = pet.Name;
            NumberOfPaws = pet.NumberOfPaws;
        }

        public virtual string AsksToEat() => "Домашнее животное просит кушать";

        public object Clone() => MyClone();

        public virtual Pet MyClone() => new(this);

        public override string ToString() =>
            $"Age: {Age} | Weight: {Weight} | Name: {Name} | NumberOfPaws: {NumberOfPaws}, {AsksToEat()}";
    }
}
