﻿using Prototype.Abstractions;

namespace Prototype.Models
{
    public class Cat : Pet, IMyCloneable<Cat>, ICloneable
    {
        /// <summary>
        /// Признак наличия когтей
        /// </summary>
        public bool HasClaws { get; set; }

        public Cat() { }

        public Cat(Cat cat) : base(cat.Age, cat.Weight, cat.Name, cat.NumberOfPaws)
        {
            HasClaws = cat.HasClaws;
        }

        public Cat(int age, int weight, string name, int numberOfPaws, bool hasClaws) : base(age, weight, name, numberOfPaws)
        {
            NumberOfPaws = numberOfPaws;
            HasClaws = hasClaws;
        }

        public override string AsksToEat() => "Котик просит кушать";

        public override Cat MyClone()
        {
            return new Cat(this);
        }

        public override string ToString() =>
            $"Age: {Age} | Weight: {Weight} | Name: {Name} | NumberOfPaws: {NumberOfPaws}, HasClaws: {HasClaws}, {AsksToEat()}";
    }
}
